Mopidy>=3.0.0
Mopidy-Podcast>=3.0.0
Pykka>=2.0.1
requests>=2.0
setuptools

[dev]
black
check-manifest
flake8
flake8-black
flake8-bugbear
flake8-import-order
isort[pyproject]
twine
wheel
pytest
pytest-cov
responses

[lint]
black
check-manifest
flake8
flake8-black
flake8-bugbear
flake8-import-order
isort[pyproject]

[release]
twine
wheel

[test]
pytest
pytest-cov
responses
